<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;

class NavigationController extends Controller
{
    public function home(Request $request)
    {
        return Inertia::render('Guest/Home', [
            /*'event' => $event->only(
                'id',
                'title',
                'start_date',
                'description'
            ),*/
        ]);
    }
}
